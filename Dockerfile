# Use an official nginx runtime as a parent image
FROM nginx:alpine

RUN apk update && apk add git

# Set the working directory to the nginx document root
WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

# Copy the contents of the local 'html' directory to the working directory in the container
#COPY static-website-example/* .
RUN git clone https://github.com/diranetafen/static-website-example.git .

# Expose port 80 so that the container can be accessed from the outside
EXPOSE 80

