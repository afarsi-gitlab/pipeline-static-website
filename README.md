# pipeline-static-website


## The main goal:
We have a static web application developped in HTML, CSS and Javascript. Its source code can be found on this github link : https://github.com/diranetafen/static-website-example.git. The objective of this project is to define the CI/CD pipeline on gitlab-ci starting from building the application until its deployment on the prod environment. The figure bellow illustrates the different stages that we should define to realize the required CI/CD pipeline. 

// the given image


Through this project I'd explain how to configure a CI/CD pipeline to realease this new code changing to the production environment, where the end user can access them. 

Gitlab strieves to become a complete Devops Plateform, CI/CD is a core of the Devops that automatically and continuously testing, building and releasing code changing to the deplyment environment.   

That's mean that when an end user commit a new code into the gitlab repository, the gitlab will automatically execute a CI/CD. With Gitlab keep code management and CI/CD on the same plateform.

the Gitlab server (instance ) hosts the application code and the pipeline, however the execution of the pipeline on agants called runners able to execute CI/CD jobs

Let's start by creating our project on gitlab.com and naming it "**pipeline-static-website**". We should make it public to allow to the images created by the pipeline's job to be used by third part applications.  

we first need to contenairize our static web application. This task can be acheverd through writing the Dockerfile that allows us to create a docker image, that we run it to launch our app.

```
# Use an official nginx runtime as a parent image
FROM nginx:alpine
RUN apk update && apk add git
# Set the working directory to the nginx document root
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
# Copy the contents of the local 'html' directory to the working directory in the container
#COPY static-website-example/* .
RUN git clone https://github.com/diranetafen/static-website-example.git .
# Expose port 80 so that the container can be accessed from the outside

EXPOSE 80

CMD ["nginx", "-g", "daemon"]

```

At the aim to test our images to be sure that it works correctely:

Firstly, we buid it by executing the command below : 


```
    docker build -t afa-static-webapp-image .

```

Secondly, we run it whitin a docker container by executing the following command :

```
    docker run -it -d --name afa-static-webapp afa-static-webapp-image  
```
Thridly, we type the url on the web browser: http://127.0.0.1 to get the home page of our application as shown in the following figure :

// Figure shows the homepage of the dockerized web app

//  ici l'image 



## How Do We Create a CI/CD Gitlab pipeline ?

For following the concept of Configuration As Code, the whole pipeline will be written in code and hosted in the application git repository in a simple yaml file called _**.gitlab-ci.yaml**_. So the Gitlab can detect automatically the pipeline code and execute it without any extra configuration. 

So we create in the root of our project the file _**.gitlab-ci.yaml**_ and we either : 
- apply a predefined template to this file and keep only the code that we need as shown in the figure 2. 
- Or, write from scratch the pipeline configurtation.

We prefer the second alternative to detail each element in the pipelinec configuration file by do that directely in the gitlab-ui.

- Let's define an arbitrary name of the test job to be **run_tests**.
- The job definition must contain at least the _script_ clause
- the _script_ specifies the commands to execute


In our case, we chose Docker as template

![IMAGE_DESCRIPTION](images/project-creation.png)

We edit the .gitlab-ci.yaml 

```
image: docker:latest 
services: 
- name: docker:dind 
  alias: docker 
stages: 
  - Build image 
  - Test image

```

In this project we use a free docker runner.


Docker in Docker (DinD) is a technique that allows us to run a Docker container inside another Docker container. t enables you to build and test Docker images inside a containerized environment without the need for a separate Docker host.

